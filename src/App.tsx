import React, {useMemo} from 'react';
import {Image, ImageSourcePropType, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {AppProvider} from './AppContext';
import DashboardScreen from './screens/DashboardScreen';
import ProfileScreen from './screens/ProfileScreen';
import MarketDataScreen from './screens/MarketDataScreen';
import {colors} from './helpers/colors';

const {activeColor, inactiveColor} = colors;

type TabBarItem = {
    name: string;
    component: React.ComponentType<any>;
    title: string;
    iconSource: ImageSourcePropType;
};

const tabsData: TabBarItem[] = [
    {
        name: 'Dashboard',
        component: DashboardScreen,
        title: 'Dashboard',
        iconSource: require('../assets/dashboard.png'),
    },
    {
        name: 'Market Data',
        component: MarketDataScreen,
        title: 'Market Data',
        iconSource: require('../assets/market_data.png'),
    },
    {
        name: 'Profile',
        component: ProfileScreen,
        title: 'Profile',
        iconSource: require('../assets/profile.png'),
    },
];

const Tab = createBottomTabNavigator();

const App = () => {
    const memoizedTabs = useMemo(() => tabsData.map(buildTab), []);

    return (
        <AppProvider>
            <NavigationContainer>
                <Tab.Navigator
                    screenOptions={{
                        tabBarStyle: styles.tabBar,
                        tabBarActiveTintColor: activeColor,
                        tabBarInactiveTintColor: inactiveColor,
                    }}>
                    {memoizedTabs}
                </Tab.Navigator>
            </NavigationContainer>
        </AppProvider>
    );
};

const buildTab = ({name, component, title, iconSource}: TabBarItem) => (
    <Tab.Screen
        options={{
            tabBarLabel: title,
            tabBarIcon: ({focused}) => (
                <Image
                    source={iconSource}
                    style={[
                        styles.icon,
                        {tintColor: focused ? activeColor : inactiveColor},
                    ]}
                />
            ),
        }}
        name={name}
        component={component}
        key={name}
    />
);

const styles = StyleSheet.create({
    tabBar: {
        height: 72,
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 22,
    },
    icon: {
        width: 24,
        height: 24,
    },
});

export default App;
