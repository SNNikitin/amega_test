import React, {useState, useCallback, memo, useRef, useEffect} from 'react';
import {Dimensions, StyleSheet, Text, View, FlatList} from 'react-native';
import {LineChart} from 'react-native-svg-charts';
import * as shape from 'd3-shape';
import throttle from 'lodash.throttle';

import showError from '../helpers/ShowError.ts';
import {marketUrl} from '../helpers/configs.ts';
import {colors} from '../helpers/colors.ts';
import {Loader} from '../components';

import {useFocusEffect} from '@react-navigation/native';

const contentInset = {top: 20, bottom: 20};

const getMinValue = (minPrice: number | null) =>
    minPrice !== null ? minPrice * 0.999999 : 0;
const getMaxValue = (maxPrice: number | null) =>
    maxPrice !== null ? maxPrice * 1.000001 : 100;

const formatCurrency = (value: number) =>
    value.toLocaleString('en-US', {style: 'currency', currency: 'USD'});

interface PriceItemProps {
    item: number;
}

interface State {
    priceHistory: number[];
    minPrice: number | null;
    maxPrice: number | null;
    messageCount: number;
    displayedPrices: number[];
}

const PriceItem: React.FC<PriceItemProps> = memo(({item}) => (
    <Text style={styles.priceText}>{formatCurrency(item)}</Text>
));

const MarketDataScreen: React.FC = () => {
    const [state, setState] = useState<State>({
        priceHistory: [],
        minPrice: null,
        maxPrice: null,
        messageCount: 0,
        displayedPrices: [],
    });

    const throttledHandler = throttle((event: WebSocketMessageEvent) => {
        const data = JSON.parse(event.data);
        const price = parseFloat(data.p);

        setState(prevState => {
            const newPrices = [...prevState.priceHistory, price].slice(-20);
            const newMinPrice = Math.min(...newPrices);
            const newMaxPrice = Math.max(...newPrices);

            return {
                ...prevState,
                priceHistory: newPrices,
                minPrice: newMinPrice,
                maxPrice: newMaxPrice,
                messageCount: prevState.messageCount + 1,
                displayedPrices: newPrices,
            };
        });
    }, 100);

    const handleOnMessage = useCallback(throttledHandler, [throttledHandler]);

    const ws = useRef<WebSocket | null>(null);

    const handleOnMessageRef = useRef(handleOnMessage);

    useEffect(() => {
        handleOnMessageRef.current = handleOnMessage;
    }, [handleOnMessage]);

    useFocusEffect(
        useCallback(() => {
            ws.current = new WebSocket(marketUrl);

            ws.current.onmessage = event => handleOnMessageRef.current(event);

            ws.current.onerror = () => showError;

            return () => {
                if (ws.current) {
                    ws.current.close();
                    ws.current = null;
                }
            };
        }, []),
    );

    return (
        <View style={styles.container}>
            {state.priceHistory.length > 0 ? (
                <View style={styles.body}>
                    <View style={styles.chartContainer}>
                        <LineChart
                            style={styles.chart}
                            data={state.priceHistory}
                            svg={{stroke: styles.chartColor.color}}
                            curve={shape.curveStep}
                            contentInset={contentInset}
                            yMin={getMinValue(state.minPrice)}
                            yMax={getMaxValue(state.maxPrice)}
                        />
                    </View>
                    <Text style={styles.header}>BTC/USDT</Text>
                    <FlatList
                        data={state.displayedPrices}
                        style={styles.list}
                        keyExtractor={(_, index) => index.toString()}
                        renderItem={({item}) => <PriceItem item={item} />}
                        removeClippedSubviews
                        maxToRenderPerBatch={20}
                        windowSize={10}
                    />
                </View>
            ) : (
                <Loader />
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    body: {
        margin: 16,
        gap: 16,
    },
    chartContainer: {
        flexDirection: 'row',
    },
    chart: {
        height: Dimensions.get('window').height / 3,
        width: Dimensions.get('window').width - 32,
        backgroundColor: colors.white,
    },
    chartColor: {
        color: colors.inactiveColor,
    },
    header: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 8,
        width: '100%',
        alignSelf: 'center',
        color: colors.text,
    },
    list: {
        width: 120,
        alignSelf: 'center',
        marginVertical: 0,
    },
    priceText: {
        fontSize: 16,
        fontFamily: 'monospace',
        alignSelf: 'center',
        color: colors.text,
    },
});

export default MarketDataScreen;
