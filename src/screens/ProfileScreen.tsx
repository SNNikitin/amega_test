import React, {useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {AppContext} from '../AppContext.tsx';
import {LocationInfo, ImageItem} from '../components';
import {colors} from '../helpers/colors.ts';

const ProfileScreen: React.FC = () => {
    const {ipData, selectedImage} = useContext(AppContext);

    return (
        <View style={styles.body}>
            {ipData ? (
                <LocationInfo ipData={ipData} />
            ) : (
                <Text style={styles.text}>
                    Please submit IP address on the Dashboard Screen
                </Text>
            )}
            {selectedImage ? (
                <ImageItem item={selectedImage} />
            ) : (
                <Text style={styles.text}>
                    Please select an image on the Dashboard Screen
                </Text>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    body: {
        margin: 16,
        gap: 32,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    text: {
        color: colors.text,
    },
});

export default ProfileScreen;
