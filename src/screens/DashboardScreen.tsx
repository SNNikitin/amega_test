import React, {
    useState,
    useEffect,
    useContext,
    useCallback,
    useMemo,
} from 'react';
import {
    View,
    Text,
    TextInput,
    FlatList,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
} from 'react-native';
import ipRegex from 'ip-regex';

import {AppContext} from '../AppContext.tsx';
import {IpData} from '../types/IpData.ts';
import showError from '../helpers/ShowError.ts';
import {colors} from '../helpers/colors.ts';
import {imagesData, ipUrl} from '../helpers/configs.ts';
import {LocationInfo, ImageItem, Loader} from '../components';

const fetchIpData = (
    ip: string | undefined = '',
    setData: (data: IpData) => void,
) => {
    fetch(`${ipUrl}${ip}`)
        .then(response => response.json())
        .then(data => setData(data))
        .catch(showError);
};

const DashboardScreen: React.FC = () => {
    const {setIpData, setSelectedImage} = useContext(AppContext);
    const [inputIp, setInputIp] = useState('');
    const [isInputIpValid, setIsInputIpValid] = useState(true);
    const [images] = useState<string[]>(imagesData);
    const [localIpData, setLocalIpData] = useState<IpData>();

    useEffect(() => {
        fetchIpData(undefined, setLocalIpData);
    }, []);

    const handleTextChange = useCallback((text: string) => {
        setInputIp(text);
        setIsInputIpValid(text === '' || ipRegex({exact: true}).test(text));
    }, []);

    const handleIpSubmit = useCallback(() => {
        fetchIpData(inputIp, data => {
            if (data) {
                setInputIp(data?.ip!);
                setLocalIpData(data);
                setIpData(data);
            }
        });
    }, [inputIp, setIpData]);

    const handleImagePress = useCallback(
        (image: string) => {
            setSelectedImage(image);
        },
        [setSelectedImage],
    );

    const imageList = useMemo(
        () => (
            <FlatList
                horizontal
                data={images}
                showsHorizontalScrollIndicator={false}
                renderItem={({item}) => (
                    <ImageItem item={item} onPress={handleImagePress} />
                )}
                keyExtractor={item => item}
            />
        ),
        [images, handleImagePress],
    );

    return (
        <ScrollView
            keyboardShouldPersistTaps="never"
            scrollEnabled={false}
            style={styles.body}>
            <View style={styles.location}>
                {localIpData ? (
                    <LocationInfo ipData={localIpData} />
                ) : (
                    <Loader />
                )}
            </View>
            <TextInput
                value={inputIp}
                onChangeText={handleTextChange}
                keyboardType="numeric"
                placeholder="Enter IP Address"
                placeholderTextColor={colors.inputBorderColor}
                style={styles.input}
            />
            <Text style={styles.infoText}>
                If you do not enter an IP, the recognized value of your location
                will be used.
            </Text>
            <TouchableOpacity
                style={styles.button}
                onPress={handleIpSubmit}
                disabled={!isInputIpValid}>
                <Text
                    style={
                        isInputIpValid
                            ? styles.buttonLabel
                            : styles.buttonLabelDisabled
                    }>
                    Submit {inputIp || localIpData?.ip}
                </Text>
            </TouchableOpacity>
            {imageList}
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    body: {
        gap: 16,
    },
    location: {
        width: '100%',
        height: 120,
        justifyContent: 'center',
        marginHorizontal: 16,
    },
    input: {
        marginHorizontal: 16,
        padding: 8,
        height: 44,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: colors.inputBorderColor,
        color: colors.text,
    },
    infoText: {
        marginTop: 4,
        marginLeft: 16,
        fontSize: 11,
        color: colors.inputBorderColor,
    },
    button: {
        margin: 16,
        height: 44,
        paddingHorizontal: 16,
        flexDirection: 'row',
        borderRadius: 22,
        backgroundColor: colors.buttonBackgroundColor,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    buttonLabel: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.activeColor,
    },
    buttonLabelDisabled: {
        fontSize: 14,
        color: colors.inactiveColor,
    },
    image: {
        width: 250,
        height: 250,
        margin: 8,
    },
    text: {
        marginBottom: 8,
        color: colors.text,
    },
    bold: {
        fontWeight: 'bold',
        color: colors.text,
    },
});

export default DashboardScreen;
