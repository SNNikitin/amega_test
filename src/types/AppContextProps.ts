export interface AppContextProps {
    ipData: any;
    setIpData: (data: any) => void;
    selectedImage: string | null;
    setSelectedImage: (image: string | null) => void;
}
