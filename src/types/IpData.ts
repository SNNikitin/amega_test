export interface IpData {
    ip?: string;
    connection?: {
        isp?: string;
    };
    continent?: string;
    country?: string;
    flag?: {
        emoji?: string;
    };
    city?: string;
    timezone?: {
        id?: string;
        abbr?: string;
        utc?: string;
    };
}
