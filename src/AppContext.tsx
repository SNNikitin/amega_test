import React, {createContext, useState, ReactNode, useCallback} from 'react';
import {AppContextProps} from './types/AppContextProps';

const defaultValue: AppContextProps = {
    ipData: null,
    setIpData: () => {},
    selectedImage: null,
    setSelectedImage: () => {},
};

export const AppContext = createContext<AppContextProps>(defaultValue);

export const AppProvider = ({children}: {children: ReactNode}) => {
    const [ipData, setIpData] = useState(null);
    const [selectedImage, setSelectedImage] = useState<string | null>(null);

    const updateIpData = useCallback((data: any) => {
        setIpData(data);
    }, []);

    const updateSelectedImage = useCallback((image: string | null) => {
        setSelectedImage(image);
    }, []);

    return (
        <AppContext.Provider
            value={{
                ipData,
                setIpData: updateIpData,
                selectedImage,
                setSelectedImage: updateSelectedImage,
            }}>
            {children}
        </AppContext.Provider>
    );
};
