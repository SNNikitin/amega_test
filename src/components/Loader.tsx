import React from 'react';
import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import {colors} from '../helpers/colors.ts';

export const Loader: React.FC = () => (
    <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color={colors.inputBorderColor} />
        <Text style={styles.loadingText}>Loading...</Text>
    </View>
);

const styles = StyleSheet.create({
    loadingContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    loadingText: {
        marginTop: 16,
        fontSize: 18,
        color: colors.inputBorderColor,
    },
});
