import React, {memo} from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import {colors} from '../helpers/colors.ts';

interface ImageItemProps {
    item: string;
    onPress?: (item: string) => void;
}

export const ImageItem = memo(({item, onPress}: ImageItemProps) => {
    if (onPress) {
        return (
            <TouchableOpacity onPress={() => onPress(item)}>
                <Image source={{uri: item}} style={styles.image} />
            </TouchableOpacity>
        );
    }
    return (
        <View>
            <Image source={{uri: item}} style={[styles.image, styles.static]} />
        </View>
    );
});

const styles = StyleSheet.create({
    image: {
        width: 300,
        height: 300,
        margin: 8,
    },
    static: {
        borderRadius: 12,
        borderWidth: 4,
        borderColor: colors.inactiveColor,
    },
});
