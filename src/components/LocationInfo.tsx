import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {IpData} from '../types/IpData.ts';
import {colors} from '../helpers/colors.ts';

interface RenderIpDataProps {
    label: string;
    value: string | undefined;
}

const RenderIpData: React.FC<RenderIpDataProps> = ({label, value}) => (
    <Text style={styles.text}>
        <Text style={styles.bold}>{label}:</Text> {value ?? '...'}
    </Text>
);

export const LocationInfo = (props: {ipData?: IpData}) => {
    const {ipData} = props;

    if (!ipData) {
        return null;
    }

    return (
        <View>
            <RenderIpData label="IP Address" value={ipData?.ip} />
            <RenderIpData label="ISP" value={ipData?.connection?.isp} />
            <RenderIpData
                label="Location"
                value={
                    ipData
                        ? `${ipData.continent}, ${ipData.country} (${ipData.flag?.emoji}), ${ipData.city}`
                        : undefined
                }
            />
            <RenderIpData
                label="Timezone"
                value={
                    ipData
                        ? `${ipData.timezone?.id}, ${ipData.timezone?.abbr} (UTC ${ipData.timezone?.utc})`
                        : undefined
                }
            />
        </View>
    );
};

const styles = StyleSheet.create({
    text: {
        marginBottom: 8,
        color: colors.text,
    },
    bold: {
        fontWeight: 'bold',
        color: colors.text,
    },
});
