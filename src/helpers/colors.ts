export const colors = {
    activeColor: '#0011AA',
    inactiveColor: '#8899EE',
    inputBorderColor: '#7799BB',
    buttonBackgroundColor: '#AACCFF',
    white: '#FFFFFF',
    text: '#002233',
};
