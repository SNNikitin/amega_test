import {Alert} from 'react-native';

export default () => Alert.alert('Error', 'Failed to fetch IP data.');
